#include "siteview.h"
#include <Qt3D/qglbuilder.h>
#include <QMouseEvent>
#include <QStyleOptionRubberBand>

CustomRB::CustomRB(Shape s, QWidget *parent) : QRubberBand(s, parent)
{
    setWindowFlags(Qt::ToolTip);

    /*QStyleOptionRubberBand styleOption;
    styleOption.opaque = false;
    initStyleOption(&styleOption);*/
}

void CustomRB::paintEvent(QPaintEvent *event)
{
    QPainter painter;
    QPen pen = QPen(Qt::yellow);
    pen.setWidth(3);
    pen.setStyle(Qt::DashLine);

    QBrush brush = QBrush(Qt::red);

    painter.begin(this);
    painter.setPen(pen);
    //painter.setOpacity(0.1);
    painter.setBrush(brush);
    painter.drawRect(event->rect());
    painter.end();
}

SiteView::SiteView(QString siteFilePath, QWidget *parent)
    : QGLView(parent), m_lightModel(0), m_lightParameters(0), selObjsList(0), /*rubberBand(0),*/ rubberBand2(0)
{
    this->makeCurrent();
    setMinimumSize(800, 600);
    //setMinimumSize(1890, 875);
    sceneManager = new SceneManager(siteFilePath, this);

    setOption(QGLView::ObjectPicking, false);

    mainWindow = parent;
    lfBtnPressed = false;

    QGLCamera *theCam = camera();
    theCam->setEye(QVector3D(0, 0, 2500));
    theCam->setFarPlane(5000);

    //setAutoFillBackground(false);

    connect(sceneManager, SIGNAL(changed()), this, SLOT(updateGL()));
}

void SiteView::initializeGL(QGLPainter *painter)
{
    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);
    //static GLfloat lightPosition[4] = { 0.5, 5.0, 7.0, 1.0 };
    //glLightfv(GL_LIGHT0, GL_POSITION, lightPosition);

    if (!m_lightModel)
        m_lightModel = new QGLLightModel(this);
    m_lightModel->setAmbientSceneColor(Qt::white);//QColor(196, 196, 196));
    painter->setLightModel(m_lightModel);

    if (!m_lightParameters)
        m_lightParameters = new QGLLightParameters(this);
    m_lightParameters->setPosition(QVector3D(0.0, 0.0, 2500.0));
    m_lightParameters->setQuadraticAttenuation(145.8);
    painter->setMainLight(m_lightParameters);

    //glBindTexture(GL_TEXTURE_2D, sceneManager->svgScene->dynamicTexture);
    glEnable(GL_DEPTH);
    glEnable(GL_DEPTH_TEST);

    sceneManager->scene->initialize(this, painter);
}

void SiteView::paintGL(QGLPainter *painter)
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    sceneManager->scene->draw(painter);
}

void SiteView::standardLighting()
{
    sceneManager->changeMaterials(false);
    updateGL();
}

void SiteView::perPixelLighting()
{
    sceneManager->changeMaterials(true);
    updateGL();
}

void SiteView::keyPressEvent(QKeyEvent *e)
{
    sceneManager->changeMaterials(true);
    updateGL();

    if (e->key() == Qt::Key_Tab)
    {
        // The Tab key turns the ShowPicking option on and off,
        // which helps show what the pick buffer looks like.
        setOption(QGLView::ShowPicking, ((options() & QGLView::ShowPicking) == 0));
        updateGL();
    }
    QGLView::keyPressEvent(e);
}

void SiteView::unSelectAllItems()
{
    if(selObjsList)
    {
        for(int i=0, n=selObjsList->size(); i<n; i++)
        {
            MeshObject *mObj = (MeshObject*)selObjsList->at(i);
            mObj->setSelected(false);
        }
        delete selObjsList;
        selObjsList = 0;
    }
}

void SiteView::mousePressEvent(QMouseEvent *e)
{
    lfBtnPressed = e->button()==Qt::LeftButton;

    if(e->modifiers()==Qt::NoModifier)
    {
        unSelectAllItems();
        m_pos = e->pos();
        updateGL();
        return;
    }

    panning = true;
    lastPan = startPan = e->pos();
    startEye = camera()->eye();
    startCenter = camera()->center();
    startUpVector = camera()->upVector();
    setCursor(Qt::ClosedHandCursor);

    //QGLView::mousePressEvent(e);
}

#include <Qt3D/qplane3d.h>
void SiteView::screenToGL(QPoint pos, QPoint pos1, QVector3D *vect, QVector3D *vect1)
{
    QGLCamera *theCam = camera();

    //theCam->rotateEye(quat);

    QVector3D pick = mapPoint(pos);
    QVector3D pick1 = mapPoint(pos1);

    /**vect = pick;
    vect->setZ(-1);
    *vect1 = pick1;
    vect1->setZ(-1);
    return;*/

    // find the origin of the near plane
    QRay3D eyeline(theCam->eye(), (theCam->center() - theCam->eye()).normalized());
    QVector3D nearPlaneOrigin = eyeline.point(theCam->nearPlane());

    // from the near plane origin move up and across by the pick's XY to find the point
    // on the near plane
    QRay3D up(nearPlaneOrigin, theCam->upVector());
    QVector3D sideDir = QVector3D::crossProduct(theCam->upVector(), -eyeline.direction());
    QRay3D side(up.point(pick.y()), sideDir.normalized());
    QVector3D v = side.point(pick.x());
    QRay3D side1(up.point(pick1.y()), sideDir.normalized());
    QVector3D v1 = side1.point(pick1.x());

    // intersect the ray thru the picked point on the near plane with the floor
    QRay3D ray(theCam->eye(), v - theCam->eye());
    QRay3D ray1(theCam->eye(), v1 - theCam->eye());
    QPlane3D floorPlane(QVector3D(), QVector3D(0, 0, 1));

    *vect = ray.point(floorPlane.intersection(ray));
    *vect1 = ray1.point(floorPlane.intersection(ray1));

    //theCam->rotateEye(quat2);
}

#include "mainwindow.h"
#include <Qt3D/qgraphicsrotation3d.h>
#include <QtCore/qmath.h>
void SiteView::mouseMoveEvent(QMouseEvent *e)
{
    int m = e->modifiers();

    /*if(m == Qt::AltModifier)
    {
        e->setModifiers(Qt::NoModifier);
    }*/
    if(!lfBtnPressed)
    {
        if(mainWindow)
        {
            QString statusBarMsg = QString("X=%1, Y=%2").arg(QString::number(e->pos().x()), QString::number(e->pos().y()));
            ((MainWindow*)mainWindow)->setStatusBarMessage(statusBarMsg);
        }
        return;
    }

    if(m == Qt::NoModifier)
    {
        /*if(rubberBand)
        {
            sceneManager->scene->deleteMember(rubberBand);
            delete rubberBand;
            rubberBand = 0;
        }*/

        unSelectAllItems();

        //QVector3D worldPos1, worldPos2;
        //screenToGL(m_pos, e->pos(), &worldPos1, &worldPos2);
        /*worldPos1.setX(m_pos.x());
        worldPos1.setY(m_pos.y());
        worldPos2.setX(e->pos().x());
        worldPos2.setY(e->pos().y());*/

        //qreal sf = 2.3 ;
        //int xSel = m_pos.x()*sf + sceneManager->sceneSize.width()/2;
        //int ySel = -m_pos.y()*sf + sceneManager->sceneSize.height()/2;
        //QRect rect(xSel, ySel, /*qAbs*/(e->pos().x()-m_pos.x())*sf, /*qAbs*/(-e->pos().y()+m_pos.y())*sf);
        /*QRect rect(-worldPos1.x() + sceneManager->sceneSize.width()/2, -worldPos1.y() + sceneManager->sceneSize.height()/2, worldPos2.x()-worldPos1.x(), worldPos2.y()-worldPos1.y());
        rect = rect.normalized();*/
        //m_pos = parentWidget()->map m_pos);
        QPoint p2 = /*mapFromParent(*/e->pos();
        QSize s = minimumSize();
        if(p2.x()>s.width()) p2.setX(s.width());
        if(p2.x()<0) p2.setX(0);
        if(p2.y()>s.height()) p2.setY(s.height());
        if(p2.y()<0) p2.setY(0);
        QRect rect = QRect(m_pos, p2).normalized();
        QList<QGraphicsItem *> gItemList = sceneManager->graphicsScene->items(rect, Qt::IntersectsItemShape, Qt::AscendingOrder);
        selObjsList = new QList<QObject*>;//objectsForRegion(m_pos, e->pos());
        for(int i=0, n=gItemList.size(); i<n; i++)
        {
            QGraphicsItem *gItem = gItemList.at(i);
            selObjsList->append(gItem->data(0).value<MeshObject*>());
        }

        for(int i=0, n=selObjsList->size(); i<n; i++)
        {
            MeshObject *mObj = (MeshObject*)selObjsList->at(i);
            mObj->setSelected(true);
        }

        if(!selObjsList->isEmpty())
            updateGL();

        //camera()->rotateEye(quat);
        /*QGeometryData quad;
        qreal w = worldPos2.x()-worldPos1.x();
        qreal h = worldPos2.y()-worldPos1.y();

        qreal x1 = w*qCos(m_angle) - h*qSin(m_angle);
        qreal y1 = w*qSin(m_angle) + h*qCos(m_angle);

        quad.appendVertex(QVector3D(0, 0, 0));
        quad.appendVertex(QVector3D(0, y1, 0));
        quad.appendVertex(QVector3D(x1, y1, 0));
        quad.appendVertex(QVector3D(x1, 0, 0));

        quad.appendNormal(QVector3D(0, 0, 1));
        quad.appendNormal(QVector3D(0, y1, 1));
        quad.appendNormal(QVector3D(x1, y1, 1));
        quad.appendNormal(QVector3D(x1, 0, 1));

        QGLBuilder builder;
        builder.addQuads(quad);
        QGLSceneNode *rubberBandNode = builder.finalizedSceneNode();*/
        //QGraphicsRotation3D rot3d;
        //rot3d.setAngle(m_angle);
        //rot3d.setAxis(QVector3D(worldPos2.x(), worldPos2.y(), 1));
        //rubberBandNode->addTransform(&rot3d);
        /*rubberBand = new MeshObject(rubberBandNode);
        rubberBand->setMaterial(sceneManager->rubMaterial);
        rubberBand->setHoverMaterial(sceneManager->rubMaterial);
        rubberBand->setTransparent(true);
        rubberBand->setRubberBand(true);
        rubberBand->setStandardEffect(QGL::LitMaterial);
        rubberBand->setPosition(worldPos1);
        sceneManager->scene->addMember(rubberBand);*/
        //camera()->rotateEye(quat2);

        if (!rubberBand2)
        {
            rubberBand2 = new CustomRB(QRubberBand::Rectangle, this);
        }
        rubberBand2->setGeometry(rect);
        rubberBand2->show();

        return;
    }

    QPoint delta = e->pos() - startPan;
    if(m == Qt::AltModifier)
    {
        camera()->setEye(startEye);
        camera()->setCenter(startCenter);
        camera()->setUpVector(startUpVector);
    }
    else
    {
        startPan = lastPan;
        delta = e->pos() - startPan;
        startEye = camera()->eye();
        startCenter = camera()->center();
        startUpVector = camera()->upVector();
    }
    lastPan = e->pos();
    if(m == Qt::ControlModifier)
        pan(delta.x()*1000, delta.y()*1000);
    else
        if(m == Qt::AltModifier)
            rotate(delta.x());

    //QGLView::mouseMoveEvent(e);
}

void SiteView::mouseReleaseEvent(QMouseEvent *e)
{
    lfBtnPressed = false;

    if(e->modifiers() == Qt::NoModifier && e->button()==Qt::LeftButton)
    {
        /*if(rubberBand)
        {
            sceneManager->scene->deleteMember(rubberBand);
            delete rubberBand;
            rubberBand = 0;
            updateGL();
        }*/
        if(rubberBand2)
        {
            rubberBand2->hide();
            //updateGL();
        }

        return;
    }

    panning = false;
    unsetCursor();

    //QGLView::mouseReleaseEvent(e);
}

// Zoom in and out according to the change in wheel delta.
/*void SiteView::wheel(int delta)
{
    if (d->options & QGLView::FOVZoom) {
        //Use field-of view as zoom (much like a traditional camera)
        qreal scale = qAbs(viewDelta(delta, delta).x());
        if (delta < 0)
            scale = -scale;
        if (scale >= 0.0f)
            scale += 1.0f;
        else
            scale = 1.0f / (1.0f - scale);
        qreal fov = d->camera->fieldOfView();
        if (fov != 0.0f)
            d->camera->setFieldOfView(d->camera->fieldOfView() / scale);
        else
            d->camera->setViewSize(d->camera->viewSize() / scale);
    } else {
        // enable this to get wheel navigation that actually zooms by moving the
        // camera back, as opposed to making the angle of view wider.
        QVector3D viewVector= camera()->eye() - camera()->center();
        qreal zoomMag = viewVector.length();
        qreal zoomIncrement = -float(delta) / 100.0f;
        if (!qFuzzyIsNull(zoomIncrement))
        {
            zoomMag += zoomIncrement;
            if (zoomMag < 1.0f)
                zoomMag = 1.0f;

            QRay3D viewLine(camera()->center(), viewVector.normalized());
            camera()->setEye(viewLine.point(zoomMag));
        }
    }

}*/

// Pan left/right/up/down without rotating about the object.
void SiteView::pan(int deltax, int deltay)
{
    QPointF delta = viewDelta(deltax, deltay);
    QVector3D t = camera()->translation(delta.x(), -delta.y(), 0.0f);

    // Technically panning the eye left should make the object appear to
    // move off to the right, but this looks weird on-screen where the user
    // actually thinks they are picking up the object and dragging it rather
    // than moving the eye.  We therefore apply the inverse of the translation
    // to make it "look right".
    camera()->setEye(camera()->eye() - t);
    camera()->setCenter(camera()->center() - t);
}

// Rotate about the object being viewed.
void SiteView::rotate(int deltax)
{
    m_angle = deltax * 90.0f / width();
    //quat = camera()->roll(-m_angle);
    //QQuaternion q = quat2 = camera()->roll(m_angle);
    QQuaternion q = camera()->roll(m_angle);
    camera()->rotateCenter(q);
    m_angle = -m_angle;
}

