#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QPushButton>
#include "sitedialog.h"
#include <QDoubleSpinBox>
#include <QLabel>
#include "../ClientEditorSharedSources/site2dview.h"

namespace Ui {
    class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    void setStatusBarMessage(QString msg);
    //QString server;
    //int port;

private:
    Ui::MainWindow *ui;
    QString curPath;

    //SiteDialog *siteDialog;
    //SiteView *siteView;
/*    QAction *standardLighting;
#if !defined(QT_OPENGL_ES_1)
    QAction *perPixelLighting;
#endif
*/
    QAction *openAct;
    QAction *saveAct;
    QAction *saveAsAct;
    QAction *saveAllAct;
    QAction *exportAct;
    QAction *closeAct;
    QAction *closeAllAct;
    QAction *exitAct;

    QMenu *fileMenu;
    QMenu *configMenu;
    QMenu *viewMenu;
    QMenu *recentFilesMenu;
    QMenu *helpMenu;
    QAction *indexAct;
    QAction *aboutSiteEditorAct;

    enum { MaxRecentFiles = 5 };
    QAction *recentFileActs[MaxRecentFiles];

    /*
    * Caché de iconos de los asientos. En la lista cada posición corresponde a uno de los estados posibles: libre, bloqueada, vendidad,...
    * El contenido de cada posición corresponde a las versiones de los iconos primero por nivel de escala y por cada nivel están las versiones por color.
    * La caché es creada es un nivel más alto: en mainwindow en el modo edición, scMainObj en modo venta. Esto permite compartir la caché a nivel de aplicación entre todos los planos abiertos.
    */
    QList< QMap<int, QMap<QString, QPair<QImage, QImage> > > > *stateIconsCache;//0 - Libre; 1 - Bloquée; 2 - Payée

    void createMenus();
    void createActions();
    bool loadFile(QString fileName);
    void updateRecentFileActions();

    void setCurrentFile(const QString &fileName);

private slots:
    //void pbLoadFile3DClic();

    void open();
    void openRecentFile();
    void save();
    void saveAs();
    void saveAll();
    void exportToSit();
    void closeSubWindow();
    void closeAll();
    void about();
};

#endif // MAINWINDOW_H

