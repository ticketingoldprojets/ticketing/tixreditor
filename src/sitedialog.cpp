#include "sitedialog.h"
#include "ui_sitedialog.h"
#include <QFileDialog>
#include <QMessageBox>
#include <QGraphicsScene>
#include <QMouseEvent>
#include <QFile>
/*#include <QColorDialog>
#include <QInputDialog>*/
#include "mainwindow.h"
#include <QWidgetAction>
#include <QRadioButton>
#include <QCheckBox>

#include <QTime>

SiteDialog::SiteDialog(QString actualPath, QString svgFileName, double scaleFactor, QWidget *parent) : QDialog(parent), ui(new Ui::SiteDialog), curPath(actualPath)
{
    ui->setupUi(this);
    mainWindow = parent;

    graphicsView = new ScGraphicsView_OLD(curPath, this);

    splitter = new QSplitter;
    splitter->addWidget(graphicsView);
    splitter->addWidget(ui->scrollArea);

    ui->horizontalLayout->addWidget(splitter);

    this->setLayout(ui->horizontalLayout);

    QTime t0 = QTime::currentTime();

    QFile f(svgFileName);
    f.open(QIODevice::ReadOnly);
    QByteArray svgContent = f.readAll();
    svgDomDoc.setContent(svgContent);
    f.close();

    int n = t0.msecsTo(QTime::currentTime());
    qDebug(QString("svgDomDoc.setContent: %1 ms").arg(n).toAscii().constData());

    t0 = QTime::currentTime();
    renderer = new QSvgRenderer(svgContent);
    n = t0.msecsTo(QTime::currentTime());
    qDebug(QString("QSvgRenderer(svgContent): %1 ms").arg(n).toAscii().constData());

    QGraphicsScene *scene = new QGraphicsScene(graphicsView);
    //scene->setBspTreeDepth(0);

    //connect(scene, SIGNAL(selectionChanged()), graphicsView, SLOT(itemsSelectionChanged()));

    domTreeWalker(svgDomDoc.documentElement());

    t0 = QTime::currentTime();
    for(int i=0, N=idList.size(); i<N; i++)
    {
        QString id = idList.at(i);
        ScGraphicsSvgItem *svgItem = new ScGraphicsSvgItem();
        //svgItem->setCacheMode(QGraphicsItem::ItemCoordinateCache);
        svgItem->setSharedRenderer(renderer);
        svgItem->setElementId(QLatin1String(id.toLatin1()));
        QRectF rect = renderer->matrixForElement(id).mapRect(renderer->boundsOnElement(id));
        svgItem->setPos(rect.x(), rect.y());
        //svgItem->setZValue((rect.toRect().x() + rect.toRect().y()) % 2);
        if(id.contains("MaPlace"))
        {
            svgItem->setFlags(QGraphicsItem::ItemIsSelectable);
            svgItem->setAcceptHoverEvents(true);
        }
        scene->addItem(svgItem);
        idScGraphicsSvgItemMap.insert(id, svgItem);
    }
    n = t0.msecsTo(QTime::currentTime());
    qDebug(QString("load svg items: %1 ms").arg(n).toAscii().constData());

    graphicsView->setScene(scene);
    graphicsView->scale(scaleFactor, scaleFactor);

    connect(ui->doubleSpinBox, SIGNAL(valueChanged(double)), this, SLOT(zoomScene(double)));
    connect(ui->dial, SIGNAL(valueChanged(int)), this, SLOT(rotScene(int)));
    connect(ui->spinBox, SIGNAL(valueChanged(int)), this, SLOT(rotScene2(int)));
    connect(ui->pushButton, SIGNAL(clicked()), this, SLOT(downLoadBtnClick()));
    connect(ui->pushButton_2, SIGNAL(clicked()), this, SLOT(affecterBtnClick()));
    connect(ui->pushButton_3, SIGNAL(clicked()), this, SLOT(eliminerBtnClick()));
    connect(ui->pushButton_4, SIGNAL(clicked()), this, SLOT(genererBtnClick()));
    connect(ui->treeWidget, SIGNAL(itemSelectionChanged()), this, SLOT(showCountSelectedPlaces()));

    ui->treeWidget->setHeaderLabels(QStringList()<<"Bloc"<<"Place");
    QFileInfo fi(svgFileName);
    siteName = fi.baseName();
    this->setWindowTitle(siteName);
}

SiteDialog::~SiteDialog()
{
    delete ui;
}

bool SiteDialog::setNewZoomValue(qreal value)
{
    if(value<ui->doubleSpinBox->minimum() || value>ui->doubleSpinBox->maximum() || qAbs(value-ui->doubleSpinBox->value())<0.01)
        return false;
    ui->doubleSpinBox->disconnect(this);
    ui->doubleSpinBox->setValue(value);
    connect(ui->doubleSpinBox, SIGNAL(valueChanged(double)), this, SLOT(zoomScene(double)));
    return true;
}

void SiteDialog::downLoadBtnClick()
{
    ui->pushButton->setEnabled(false);
    ui->pushButton_4->setEnabled(false);

    QString codeConfSalle = ui->lineEdit->text();

    confSalleFileName = curPath+"svg/"+codeConfSalle+".xml";

    makeRequest = true;
    if(QFile::exists(confSalleFileName))
        makeRequest = QMessageBox::question(this, "Fichier trouvé", "L'information pour cette salle existe déjŕ. Souhaitez-vous l'utiliser?", QMessageBox::Yes, QMessageBox::No) == QMessageBox::No;

    if(makeRequest)
    {
        QString server = ((MainWindow*)this->mainWindow)->server;
        QString port = QString::number(((MainWindow*)this->mainWindow)->port);

        QDomDocument requestDOM;
        QDomElement rootNode = requestDOM.createElement("Command");
        requestDOM.appendChild(rootNode);
        rootNode.setAttribute("name", "listeBlocsAndPlaces");

        QDomElement listeBlocsAndPlacesParamsElem = requestDOM.createElement("ListeBlocsAndPlacesParams");
        rootNode.appendChild(listeBlocsAndPlacesParamsElem);

        listeBlocsAndPlacesParamsElem.setAttribute("codeConfSalle", codeConfSalle);
        listeBlocsAndPlacesParamsElem.setAttribute("codeManifestation", ui->lineEdit_2->text());
        listeBlocsAndPlacesParamsElem.setAttribute("typeTarif", ui->lineEdit_3->text());
        listeBlocsAndPlacesParamsElem.setAttribute("dateSeance", ui->dateTimeEdit->dateTime().toString(Qt::ISODate));

        replyBuffer.open(QIODevice::WriteOnly);

        reply = qnam.post(QNetworkRequest(QUrl("http://"+server+":"+port)), requestDOM.toString().toUtf8());

        connect(reply, SIGNAL(finished()), this, SLOT(httpFinished()));
        connect(reply, SIGNAL(readyRead()), this, SLOT(httpReadyRead()));
    }
    else
        httpFinished();
}

void SiteDialog::affecterBtnClick()
{
    /*QColor color = QColorDialog::getColor(Qt::blue, this, "Couleur pour zone de tarif");

    if(!color.isValid())
        return;

    bool ok;
    QString text = QInputDialog::getText(this, tr("Nom de la zone de tarif"), tr("Zone de tarif:"), QLineEdit::Normal, "01", &ok);

    if(!ok || text.isEmpty())
        return;*/

    QList<QTreeWidgetItem *> treeWidgetItemList = ui->treeWidget->selectedItems();

    int sizeSC = selectedItemsIdList.size();
    int sizeFnac = treeWidgetItemList.size();

    if(sizeFnac != sizeSC)
    {
        QMessageBox::information(0, tr("Error"), tr("Il faut selectioner la męme quantité de places SimpleCLIC et Fnac."));
        return;
    }

    if(!sizeSC)
        return;

    QString allocationStr;
    for(int i=0; i<sizeSC; i++)
    {
        QString svgElemId = selectedItemsIdList.at(i);
        QDomElement svgElem = idDomElemMap.value(svgElemId);
        QString itemColor = svgElem.attribute("couleurZone");

        QTreeWidgetItem *treeWidgetItem = treeWidgetItemList.at(i);
        treeWidgetItem->setBackgroundColor(0, QColor(itemColor));
        treeWidgetItem->setBackgroundColor(1, QColor(itemColor));

        treeWidgetItem->setData(1, Qt::UserRole, QVariant(svgElemId));

        QString coord = QString("(%1,%2)").arg(svgElem.attribute("idXsc"), svgElem.attribute("idYsc"));

        allocationStr += coord + " = " + treeWidgetItem->text(1);
        if(i+1<sizeSC)
            allocationStr += "|";

        svgElem.setAttribute("siegeFnac", treeWidgetItem->data(0, Qt::UserRole).toString());

        graphicsView->listSelectedSvgItem.at(i)->changeIsPlaceFnac(true);
        graphicsView->listSelectedSvgItem.at(i)->update();
    }

    treeItemSelectionsList.append(treeWidgetItemList);

    new QListWidgetItem(allocationStr, ui->listWidget);
}

void SiteDialog::eliminerBtnClick()
{
    QList<QListWidgetItem *> selectedItemsList = ui->listWidget->selectedItems();

    for(int i=0, n=selectedItemsList.size(); i<n; i++)
    {
        QListWidgetItem *listWidgetItem = selectedItemsList.at(i);

        int itemIndex = ui->listWidget->row(listWidgetItem);
        QList<QTreeWidgetItem *> treeWidgetItemList = treeItemSelectionsList.at( itemIndex );

        for(int j=0, m=treeWidgetItemList.size(); j<m; j++)
        {
            QTreeWidgetItem *treeWidgetItem = treeWidgetItemList.at(j);
            treeWidgetItem->setBackgroundColor(0, Qt::white);
            treeWidgetItem->setBackgroundColor(1, Qt::white);

            QString svgElemId =  treeWidgetItem->data(1, Qt::UserRole).toString();
            QDomElement svgElem = idDomElemMap.value(svgElemId);
            if(svgElem.hasAttribute("siegeFnac"))
            {
                svgElem.removeAttribute("siegeFnac");
            }

            ScGraphicsSvgItem *svgItem = idScGraphicsSvgItemMap.value(svgElemId);
            svgItem->changeIsPlaceFnac(false);
            svgItem->update();

            treeWidgetItem->setData(1, Qt::UserRole, QVariant(QString::null));
        }

        treeWidgetItemList.clear();
        treeItemSelectionsList.removeAt(itemIndex);

        listWidgetItem = ui->listWidget->takeItem(itemIndex);
        if(listWidgetItem)
            delete listWidgetItem;
    }
}

void SiteDialog::genererBtnClick()
{
    int total = 0;
    for(int i=0, n=treeItemSelectionsList.size(); i<n; i++)
    {
        total += treeItemSelectionsList.at(i).size();
    }

    if(total != totalPlaces)
    {
        QMessageBox::StandardButton r = QMessageBox::question(0, tr("Information"), tr("Pas toutes les places Fnac ont été affectées. Voulez-vous continuer quand męme?"), QMessageBox::Ok|QMessageBox::Cancel);
        if(r == QMessageBox::Cancel)
            return;
    }

    ui->pushButton_4->setEnabled(false);
    ui->pushButton->setEnabled(false);

    QFile fileOut(curPath + siteName + "_ScFnac.xml");
    fileOut.open(QIODevice::WriteOnly);
    QXmlStreamWriter xmlWriter(&fileOut);
    xmlWriter.setAutoFormatting(true);
    xmlWriter.writeStartDocument();

    xmlWriter.writeStartElement("ScFnacMapping");
    xmlWriter.writeAttribute("salle", siteName);

    for(int i=0, n=treeItemSelectionsList.size(); i<n; i++)
    {
        QList<QTreeWidgetItem *> treeWidgetItemList = treeItemSelectionsList.at( i );

        for(int j=0, m=treeWidgetItemList.size(); j<m; j++)
        {
            QTreeWidgetItem *treeWidgetItem = treeWidgetItemList.at(j);

            QString itemId = treeWidgetItem->data(1, Qt::UserRole).toString();
            QDomElement elem = idDomElemMap.value(itemId);

            xmlWriter.writeStartElement("ScFnacPair");
            xmlWriter.writeAttribute("scX", elem.attribute("idXsc"));
            xmlWriter.writeAttribute("scY", elem.attribute("idYsc"));
            xmlWriter.writeAttribute("fnacBloc", treeWidgetItem->parent()->data(0, Qt::UserRole).toString());
            xmlWriter.writeAttribute("fnacSiege", treeWidgetItem->data(0, Qt::UserRole).toString());
            xmlWriter.writeEndElement();
        }
    }

    xmlWriter.writeEndElement();
    xmlWriter.writeEndDocument();
    fileOut.close();

    QString server = ((MainWindow*)this->mainWindow)->server;
    QString port = QString::number(((MainWindow*)this->mainWindow)->port);

    replyBuffer.open(QIODevice::WriteOnly);

    QFileInfo fi(fileOut.fileName());

    fileOut.open(QIODevice::ReadOnly);

    QNetworkRequest networkRequest(QUrl("http://"+server+":"+port+"/COPY/sites/"+fi.baseName()+"."+fi.completeSuffix()));
    networkRequest.setRawHeader("Content-Encoding", "deflate");

    reply = qnam.post(networkRequest, qCompress(fileOut.readAll()));
    fileOut.close();

    //fileOut.remove();

    connect(reply, SIGNAL(finished()), this, SLOT(httpFinished2()));
    connect(reply, SIGNAL(readyRead()), this, SLOT(httpReadyRead()));
}

void SiteDialog::httpFinished2()
{
    replyBuffer.close();

    reply->deleteLater();
    reply = 0;

    responseDOM.clear();
    replyBuffer.open(QIODevice::ReadOnly);
    responseDOM.setContent(&replyBuffer, false);
    replyBuffer.close();

    QDomNodeList nodeList = responseDOM.elementsByTagName("ErrorMessage");
    if(!nodeList.isEmpty())
    {
        QDomElement errorMessageElem = nodeList.at(0).toElement();
        QString msg = errorMessageElem.attribute("msg");
        if(!msg.isEmpty())
            QMessageBox::information(0, tr("Error"), msg);
    }

    ui->pushButton->setEnabled(true);
    ui->pushButton_4->setEnabled(true);
}

void SiteDialog::httpFinished()
{
    responseDOM.clear();

    if(makeRequest)
    {
        replyBuffer.close();

        reply->deleteLater();
        reply = 0;

        replyBuffer.open(QIODevice::ReadOnly);
        responseDOM.setContent(&replyBuffer, false);
        replyBuffer.close();
    }
    else
    {
        QFile f(confSalleFileName);
        f.open(QIODevice::ReadOnly);
        responseDOM.setContent(&f, false);
        f.close();
    }

    ui->treeWidget->clear();
    ui->treeWidget->setColumnCount(2);

    QDomNodeList nodeList = responseDOM.elementsByTagName("ErrorMessage");
    if(!nodeList.isEmpty())
    {
        QDomElement errorMessageElem = nodeList.at(0).toElement();
        QMessageBox::information(0, tr("Error"), errorMessageElem.attribute("msg"));
        ui->pushButton->setEnabled(true);
        return;
    }

    nodeList = responseDOM.elementsByTagName("Bloc");

    totalBlocs = 0;
    totalPlaces = 0;

    /*QFile f2(curPath+"Gradins2011TOURNEE2_ScFnac.xml");
    f2.open(QIODevice::ReadOnly);
    QDomDocument scFnacMapDom;
    scFnacMapDom.setContent(f2.readAll());
    f2.close();
    QDomNodeList nodeList3 = scFnacMapDom.documentElement().childNodes();*/

    for(int i=0, n=nodeList.size(); i<n; i++)
    {
        if(!nodeList.at(i).isElement() )
            continue;

        QDomElement blocElem = nodeList.at(i).toElement();

        QTreeWidgetItem *blocItem = new QTreeWidgetItem(ui->treeWidget);
        totalBlocs++;

        QDomNodeList nodeList2 = blocElem.elementsByTagName("Place");

        int totalPlacesInBloc = 0;
        for(int j=0, m=nodeList2.size(); j<m; j++)
        {
            if(!nodeList2.at(j).isElement() )
                continue;

            QDomElement placeElem = nodeList2.at(j).toElement();
            QTreeWidgetItem *placeItem = new QTreeWidgetItem(blocItem);
            placeItem->setText(1, "Sičge: "+placeElem.attribute("siege")+", Rang: "+placeElem.attribute("rang"));
            placeItem->setData(0, Qt::UserRole, QVariant(placeElem.attribute("fnacId")));
            totalPlaces++;
            totalPlacesInBloc++;

            /*for(int k=0, n2=nodeList3.size(); k<n2; k++)
            {
                QDomElement mapElem = nodeList3.at(k).toElement();
                if(mapElem.attribute("fnacId") == placeElem.attribute("fnacId"))
                {
                    mapElem.setAttribute("fnacIndicePlace", placeElem.attribute("indicePlace"));
                    if(mapElem.hasAttribute("indicePlace"))
                        mapElem.removeAttribute("indicePlace");
                    break;
                }
            }*/
        }

        blocItem->setText(0, blocElem.attribute("codeBloc") + QString(" (%1)").arg(totalPlacesInBloc) );
        blocItem->setData(0, Qt::UserRole, QVariant(blocElem.attribute("codeBloc")));
    }

    /*f2.open(QIODevice::WriteOnly);
    f2.write(scFnacMapDom.toString().toUtf8());
    f2.close();*/

    ui->treeWidget->setHeaderLabels(QStringList() << QString("Bloc (%1)").arg(totalBlocs) << QString("Place (%1)").arg(totalPlaces));

    if(makeRequest)
    {
        QFile f(confSalleFileName);
        f.open(QIODevice::WriteOnly);
        f.write(responseDOM.toString().toUtf8());
        f.close();
    }

    ui->pushButton_4->setEnabled(true);
    ui->pushButton->setEnabled(true);
}

void SiteDialog::httpReadyRead()
{
    replyBuffer.write(reply->readAll());
}

void SiteDialog::zoomScene(double factor)
{
    this->setCursor(Qt::WaitCursor);
    graphicsView->zoomScene(factor);
    this->setCursor(Qt::ArrowCursor);
}

void SiteDialog::rotScene(int angle)
{
    this->setCursor(Qt::WaitCursor);
    graphicsView->rotScene(angle);
    ui->spinBox->disconnect();
    ui->spinBox->setValue(angle);
    connect(ui->spinBox, SIGNAL(valueChanged(int)), this, SLOT(rotScene2(int)));
    this->setCursor(Qt::ArrowCursor);
}

void SiteDialog::rotScene2(int angle)
{
    this->setCursor(Qt::WaitCursor);
    graphicsView->rotScene(angle);
    ui->dial->disconnect();
    ui->dial->setValue(angle);
    connect(ui->dial, SIGNAL(valueChanged(int)), this, SLOT(rotScene(int)));
    this->setCursor(Qt::ArrowCursor);
}

void SiteDialog::domTreeWalker(QDomElement root)
{
    QDomElement parentElem = root.parentNode().toElement();

    while(!root.isNull())
    {
        if(root.hasAttribute("id"))
        {
            QString id = root.attribute("id");
            if(id.contains("MaPlace"))
            {
                idList << id;
                idDomElemMap.insert(id, root);
                root.setAttribute("toBePainted", "true");
                root.parentNode().toElement().setAttribute("toBePainted", "false");
            }else

            if(renderer->elementExists(id) && !root.hasChildNodes())
            {
                if(!parentElem.isNull() && parentElem.hasAttribute("toBePainted") && parentElem.attribute("toBePainted")=="false")
                {
                    idList << id;
                    root.setAttribute("toBePainted", "true");
                }
                else
                    tmpIdList << id;
            }
        }

        if(root.hasChildNodes() && !root.hasAttribute("toBePainted"))
            domTreeWalker(root.firstChildElement());

        root = root.nextSiblingElement();
    }

    if(!parentElem.isNull() && parentElem.hasAttribute("toBePainted") && parentElem.attribute("toBePainted")=="false")
        idList += tmpIdList;else

    if(!parentElem.isNull() && parentElem.hasAttribute("toBePainted") && parentElem.attribute("toBePainted")=="true")
        tmpIdList.clear();
}

void SiteDialog::selectItem(QString itemId)
{
    if(!selectedItemsIdList.contains(itemId))
    {
        QDomElement svgItemElem = idDomElemMap.value(itemId);
        selectedItemsIdList << itemId;
        QString coord = QString("(%1,%2)").arg(svgItemElem.attribute("idXsc"), svgItemElem.attribute("idYsc"));
        QString tmp = ui->lineEdit_4->text();
        if(!tmp.isEmpty())
            tmp += " | ";
        tmp += coord;
        ui->lineEdit_4->setText(tmp);
        ui->label_6->setText("Places SimpleCLIC sélectionnées ("+QString::number(selectedItemsIdList.size())+"):");
    }
}

void SiteDialog::showSelectedItemsId(QStringList itemIdList)
{
    ui->lineEdit_4->setText(itemIdList.join(" | "));
    ui->label_6->setText("Places SimpleCLIC sélectionnées ("+QString::number(itemIdList.size())+"):");
}

void SiteDialog::unSelectItem(QString itemId)
{
    if(selectedItemsIdList.contains(itemId))
    {
        QDomElement svgItemElem = idDomElemMap.value(itemId);
        selectedItemsIdList.removeOne(itemId);
        QString coord = QString("(%1,%2)").arg(svgItemElem.attribute("idXsc"), svgItemElem.attribute("idYsc"));
        QString tmp = ui->lineEdit_4->text().remove(coord);
        ui->lineEdit_4->setText(tmp);
        ui->label_6->setText("Places SimpleCLIC sélectionnées ("+QString::number(selectedItemsIdList.size())+"):");
    }
}

void SiteDialog::unSelectAllItems()
{
    selectedItemsIdList.clear();
    ui->lineEdit_4->clear();
    ui->label_6->setText("Places SimpleCLIC sélectionnées (0):");
}

void SiteDialog::showCountSelectedPlaces()
{
    ui->label_9->setText("Places Fnac sélectionnées (" + QString::number(ui->treeWidget->selectedItems().size()) + "):");
}

ScGraphicsView_OLD::ScGraphicsView_OLD(QString cPath, QWidget * parent) : QGraphicsView(parent), totalScaleFactor(0.1)
{
    setRenderHint(QPainter::Antialiasing, false);
    setDragMode(QGraphicsView::RubberBandDrag);
    setOptimizationFlags(QGraphicsView::DontSavePainterState);
    setOptimizationFlags(QGraphicsView::DontAdjustForAntialiasing);
    setViewportUpdateMode(QGraphicsView::SmartViewportUpdate);
    //setCacheMode(QGraphicsView::CacheBackground);
    //setMouseTracking(true);
    //setViewport(new QGLWidget(QGLFormat(QGL::SampleBuffers)));
    viewport()->setAttribute(Qt::WA_AcceptTouchEvents);

    svgItemRemoved = 0;
    curPath = cPath;
    selectorItem = 0;
    actualRotationAngle = 0;
    totalAngle = 0;

    siteDialog = (SiteDialog*)parent;

    QWidgetAction *gdbhAction = new QWidgetAction(&popupMenu);
    gdbhAction->setData(QVariant(1));
    QRadioButton *rb = new QRadioButton("Gauche ŕ droite ET bas en haut", this);
    connect(rb, SIGNAL(clicked()), gdbhAction, SIGNAL(triggered()));
    gdbhAction->setDefaultWidget(rb);
    connect(gdbhAction, SIGNAL(triggered()), this, SLOT(changerModeSelection()));

    QWidgetAction *gdhbAction = new QWidgetAction(&popupMenu);
    gdhbAction->setData(QVariant(2));
    rb = new QRadioButton("Gauche ŕ droite ET haut en bas", this);
    rb->setChecked(true);
    modeSelection = 2;
    connect(rb, SIGNAL(clicked()), gdhbAction, SIGNAL(triggered()));
    gdhbAction->setDefaultWidget(rb);
    connect(gdhbAction, SIGNAL(triggered()), this, SLOT(changerModeSelection()));

    QWidgetAction *dgbhAction = new QWidgetAction(&popupMenu);
    dgbhAction->setData(QVariant(3));
    rb = new QRadioButton("Droite ŕ gauche ET bas en haut", this);
    connect(rb, SIGNAL(clicked()), dgbhAction, SIGNAL(triggered()));
    dgbhAction->setDefaultWidget(rb);
    connect(dgbhAction, SIGNAL(triggered()), this, SLOT(changerModeSelection()));

    QWidgetAction *dghbAction = new QWidgetAction(&popupMenu);
    dghbAction->setData(QVariant(4));
    rb = new QRadioButton("Droite ŕ gauche ET haut en bas", this);
    connect(rb, SIGNAL(clicked()), dghbAction, SIGNAL(triggered()));
    dghbAction->setDefaultWidget(rb);
    connect(dghbAction, SIGNAL(triggered()), this, SLOT(changerModeSelection()));

    QWidgetAction *usePhysicalCoordAction = new QWidgetAction(&popupMenu);
    QCheckBox *cb = new QCheckBox("Utiliser les coordonnées physiques", this);
    cb->setChecked(true);
    connect(cb, SIGNAL(clicked()), usePhysicalCoordAction, SIGNAL(triggered()));
    usePhysicalCoordAction->setDefaultWidget(cb);
    connect(usePhysicalCoordAction, SIGNAL(triggered()), this, SLOT(changerUsePhysicalCoord()));

    popupMenu.setTitle("Mode de sélection des places");
    popupMenu.addAction(gdbhAction);
    popupMenu.addAction(gdhbAction);
    popupMenu.addAction(dgbhAction);
    popupMenu.addAction(dghbAction);
    popupMenu.addSeparator();
    popupMenu.addAction(usePhysicalCoordAction);

    usePhysicalCoord = true;
}

/*ScGraphicsView_OLD::~ScGraphicsView_OLD()
{
}*/

void ScGraphicsView_OLD::contextMenuEvent(QContextMenuEvent * event)
{
    popupMenu.exec(mapToGlobal(event->pos()));
}

void ScGraphicsView_OLD::changerModeSelection()
{
    modeSelection = ((QWidgetAction*)this->sender())->data().toInt();
    ((QRadioButton*)((QWidgetAction*)this->sender())->defaultWidget())->setChecked(true);
    popupMenu.close();

    if(!listSelectedSvgItem.isEmpty())
    {
        QList<ScGraphicsSvgItem*> tmpList(listSelectedSvgItem);

        listSelectedSvgItem.clear();
        listSelectedSvgItemId.clear();
        siteDialog->unSelectAllItems();

        for(int i=0, n=tmpList.size(); i<n; i++)
        {
           ScGraphicsSvgItem *svgItem = tmpList.at(i);
           insertListSelectedSvgItem(svgItem);
        }

        siteDialog->showSelectedItemsId(listSelectedSvgItemId);
    }
}

void ScGraphicsView_OLD::changerUsePhysicalCoord()
{
    usePhysicalCoord = ((QCheckBox*)((QWidgetAction*)this->sender())->defaultWidget())->isChecked();
    popupMenu.close();

    if(!listSelectedSvgItem.isEmpty())
    {
        QList<ScGraphicsSvgItem*> tmpList(listSelectedSvgItem);

        listSelectedSvgItem.clear();
        listSelectedSvgItemId.clear();
        siteDialog->unSelectAllItems();

        for(int i=0, n=tmpList.size(); i<n; i++)
        {
           ScGraphicsSvgItem *svgItem = tmpList.at(i);
           insertListSelectedSvgItem(svgItem);
        }

        siteDialog->showSelectedItemsId(listSelectedSvgItemId);
    }
}

void ScGraphicsView_OLD::zoomScene(double factor)
{
    resetMatrix();
    QMatrix newMtx = matrix();
    newMtx.scale(factor,factor);
    newMtx.rotate(actualRotationAngle);
    setMatrix(newMtx);
    totalScaleFactor = factor;
}

bool ScGraphicsView_OLD::viewportEvent(QEvent *event)
{
    setDragMode(QGraphicsView::NoDrag);
    switch (event->type())
    {
        case QEvent::TouchBegin:
        case QEvent::TouchUpdate:
        case QEvent::TouchEnd:
        {
            QTouchEvent *touchEvent = static_cast<QTouchEvent *>(event);
            QList<QTouchEvent::TouchPoint> touchPoints = touchEvent->touchPoints();
            //qDebug(QString::number(touchPoints.count()).toAscii().constData());
            if (touchPoints.count() == 2)
            {
                // determine scale factor
                const QTouchEvent::TouchPoint &touchPoint0 = touchPoints.first();
                const QTouchEvent::TouchPoint &touchPoint1 = touchPoints.last();
                qreal l1 = QLineF(touchPoint0.pos(), touchPoint0.lastPos()).length();
                qreal l2 = QLineF(touchPoint1.pos(), touchPoint1.lastPos()).length();
                qreal th = 5;
                if(l1>th && l2>th)//zoom
                {
                    qreal currentScaleFactor =
                            (QLineF(touchPoint0.pos(), touchPoint1.pos()).length()
                            / QLineF(touchPoint0.lastPos(), touchPoint1.lastPos()).length());
                    qreal tmpTotalScaleFactor = totalScaleFactor;
                    if (touchEvent->touchPointStates() & Qt::TouchPointReleased)
                    {
                        // if one of the fingers is released, remember the current scale
                        // factor so that adding another finger later will continue zooming
                        // by adding new scale factor to the existing remembered value.
                        tmpTotalScaleFactor *= currentScaleFactor;
                        currentScaleFactor = 1;
                    }
                    qreal f = tmpTotalScaleFactor * currentScaleFactor;
                    //qDebug(QString::number(f).toAscii().constData());
                    if(siteDialog->setNewZoomValue(f))
                    {
                        //qDebug(("Zommed: "+QString::number(f)).toAscii().constData());
                        zoomScene(f);
                    }
                }
                else//rotation
                {
                    int a = 0;
                    if(l1<=th && l2>th)
                        a = QLineF(touchPoint0.lastPos(), touchPoint1.lastPos()).angleTo(QLineF(touchPoint0.pos(), touchPoint1.pos()));
                    else
                        if(l1>th && l2<=th)
                            a = QLineF(touchPoint1.lastPos(), touchPoint0.lastPos()).angleTo(QLineF(touchPoint1.pos(), touchPoint0.pos()));
                    totalAngle += a;
                    rotScene(-totalAngle);
                    //qDebug(("Calculated: "+QString::number(a)).toAscii().constData());
                    //qDebug(("Rotated: "+QString::number(-totalAngle)).toAscii().constData());
                }
            }
            setDragMode(QGraphicsView::RubberBandDrag);
            return true;
        }
        default:
            break;
    }
    setDragMode(QGraphicsView::RubberBandDrag);
    return QGraphicsView::viewportEvent(event);
}

void ScGraphicsView_OLD::rotScene(int angle)
{
    resetMatrix();
    QMatrix newMtx = matrix();
    newMtx.scale(totalScaleFactor,totalScaleFactor);
    actualRotationAngle = angle;
    newMtx.rotate(angle);
    setMatrix(newMtx);

    if(!listSelectedSvgItem.isEmpty())
    {
        QList<ScGraphicsSvgItem*> tmpList(listSelectedSvgItem);

        listSelectedSvgItem.clear();
        listSelectedSvgItemId.clear();
        siteDialog->unSelectAllItems();

        for(int i=0, n=tmpList.size(); i<n; i++)
        {
           ScGraphicsSvgItem *svgItem = tmpList.at(i);
           insertListSelectedSvgItem(svgItem);
        }

        siteDialog->showSelectedItemsId(listSelectedSvgItemId);
    }
}

QString ScGraphicsView_OLD::newPaintedItem(QString fileOriginalItem, QColor color, QString elemId)
{
    QFile f(fileOriginalItem);
    f.open(QIODevice::ReadOnly);
    QDomDocument doc;
    doc.setContent(&f, false);
    f.close();

    doc.documentElement().firstChild().toElement().setAttribute("id", elemId);

    QRegExp rx("fill:#\\w{6}");

    QDomNodeList list = doc.elementsByTagName("path");
    for(int i=0, n=list.size(); i<n; i++)
    {
        QDomElement elem = list.at(i).toElement();
        if(!elem.isNull())
        {
            QString style = elem.attribute("style").replace(rx, "fill:"+color.name());
            elem.setAttribute("style", style);
        }
    }

    QString tmpFileName = fileOriginalItem + ".tmp";
    f.setFileName(tmpFileName);
    f.open(QIODevice::WriteOnly);
    f.write(doc.toString().toUtf8());
    f.close();

    return tmpFileName;
}

void ScGraphicsView_OLD::insertListSelectedSvgItem(ScGraphicsSvgItem *svgItem)
{
    QString itemId = svgItem->elementId();
    QDomElement svgItemElem = siteDialog->idDomElemMap.value(itemId);

    int x, y;
    if(usePhysicalCoord)
    {
        QPointF C = viewportTransform().map((svgItem->sceneBoundingRect().topLeft() + svgItem->sceneBoundingRect().bottomRight())/2);
        x = C.x();
        y = C.y();
    }
    else
    {
        x = svgItemElem.attribute("idXsc").toInt();
        y = svgItemElem.attribute("idYsc").toInt();
    }

    QString coord = QString("(%1,%2)[%3,%4]").arg(svgItemElem.attribute("idXsc"), svgItemElem.attribute("idYsc"), QString::number(x), QString::number(y));

    bool doInsert = false;
    int threshold = 5;
    for(int i=0, n=listSelectedSvgItem.size(); i<n; i++)
    {
        ScGraphicsSvgItem *svgItem2 = listSelectedSvgItem.at(i);

        int x2, y2;
        if(usePhysicalCoord)
        {
            QPointF C = viewportTransform().map((svgItem2->sceneBoundingRect().topLeft() + svgItem2->sceneBoundingRect().bottomRight())/2);
            x2 = C.x();
            y2 = C.y();
        }
        else
        {
            itemId = svgItem2->elementId();
            svgItemElem = siteDialog->idDomElemMap.value(itemId);
            x2 = svgItemElem.attribute("idXsc").toInt();
            y2 = svgItemElem.attribute("idYsc").toInt();
        }

        switch(modeSelection)
        {
            /*
            1: "Gauche ŕ droite ET bas en haut"
            2: "Gauche ŕ droite ET haut en bas"
            3: "Droite ŕ gauche ET bas en haut"
            4: "Droite ŕ gauche ET haut en bas"
            */
            case 1: doInsert = (y-threshold>y2) || (qAbs(y-y2)<threshold && x<x2-threshold); break;
            case 2: doInsert = (y<y2-threshold) || (qAbs(y-y2)<threshold && x<x2-threshold); break;
            case 3: doInsert = (y-threshold>y2) || (qAbs(y-y2)<threshold && x-threshold>x2); break;
            case 4: doInsert = (y<y2-threshold) || (qAbs(y-y2)<threshold && x-threshold>x2);
        }

        if(doInsert)
        {
            listSelectedSvgItem.insert(i, svgItem);
            listSelectedSvgItemId.insert(i, coord);
            siteDialog->selectedItemsIdList.insert(i, itemId);
            return;
        }
    }

    listSelectedSvgItem.append(svgItem);
    listSelectedSvgItemId << coord;
    siteDialog->selectedItemsIdList << itemId;
}

void ScGraphicsView_OLD::mouseMoveEvent(QMouseEvent *event)
{
    QGraphicsItem *item = itemAt(event->pos());

    ScGraphicsSvgItem *svgItem = 0;
    if(item && item->type() == ScGraphicsSvgItem::Type)
        svgItem = (ScGraphicsSvgItem*)item;

    QString statusBarMsg;
    if(svgItem)
    {
        QString id = svgItem->elementId();
        if(id.contains("MaPlace"))
        {
            QDomElement svgItemElem = siteDialog->idDomElemMap.value(id);
            statusBarMsg = QString("X=%1, Y=%2").arg(svgItemElem.attribute("idXsc"), svgItemElem.attribute("idYsc"));
            if(svgItemElem.hasAttribute("siegeFnac"))
                statusBarMsg += ", siegeFnac=" + svgItemElem.attribute("siegeFnac");
        }
    }
    ((MainWindow*)siteDialog->mainWindow)->setStatusBarMessage(statusBarMsg);

    event->ignore();
    QGraphicsView::mouseMoveEvent(event);
}

void ScGraphicsView_OLD::itemsSelectionChanged()
{
    listSelectedSvgItem.clear();
    listSelectedSvgItemId.clear();
    siteDialog->unSelectAllItems();

    QList<QGraphicsItem *> svgItemsList = scene()->selectedItems();
    for(int i=0, n=svgItemsList.size(); i<n; i++)
    {
        ScGraphicsSvgItem *svgItem = (ScGraphicsSvgItem*)svgItemsList.at(i);

        if(!listSelectedSvgItem.contains(svgItem))
        {
            insertListSelectedSvgItem(svgItem);
        }
    }
    siteDialog->showSelectedItemsId(listSelectedSvgItemId);
}

ScGraphicsSvgItem::ScGraphicsSvgItem(QGraphicsItem * parent) : QGraphicsSvgItem(parent)
{
    isSelected = false;
    isPlaceFnac = false;
    selectedColor = Qt::blue;
}

ScGraphicsSvgItem::ScGraphicsSvgItem(const QString & fileName, QGraphicsItem * parent) : QGraphicsSvgItem(fileName, parent)
{
    isSelected = false;
    isPlaceFnac = false;
    selectedColor = Qt::blue;
}

void ScGraphicsSvgItem::changeIsPlaceFnac(bool newState)
{
    isPlaceFnac = newState;
}

/*QRectF ScGraphicsSvgItem::boundingRect() const
{
    return QRectF(0, 0, 210, 170);
}

QPainterPath ScGraphicsSvgItem::shape() const
{
    QPainterPath path;
    path.addRect(14, 14, 82, 42);
    return path;
}*/

void ScGraphicsSvgItem::paint( QPainter * painter, const QStyleOptionGraphicsItem * option, QWidget * widget )
{
    Q_UNUSED(widget);

    QColor color = Qt::white;
    //QColor fillColor = (option->state & QStyle::State_Selected) ? /*color.dark(150)*/Qt::red : color;
    QColor fillColor = color;
    if(option->state & QStyle::State_Selected)
        fillColor = Qt::red;
    if (option->state & QStyle::State_MouseOver)
    {
        fillColor = /*fillColor.light(125)*/Qt::blue;
    }

    const QRectF &bounds = boundingRect();

    painter->fillRect(/*QRectF(0, 0, 110, 70)*/bounds, fillColor);

    QGraphicsSvgItem::paint(painter, option, widget);

    if(isPlaceFnac)
    {
        painter->setPen(QPen(Qt::red, 2, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
        painter->setFont(QFont("Times", 10, QFont::Bold));
        painter->drawText(bounds, "FNAC", Qt::AlignHCenter|Qt::AlignVCenter);
    }
}

