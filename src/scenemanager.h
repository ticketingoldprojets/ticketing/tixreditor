#ifndef SCENEMANAGER_H
#define SCENEMANAGER_H

#include <QObject>
#include <Qt3D/qglmaterial.h>
#include <Qt3D/qglsvg2scene.h>
#include "sceneobject.h"
#include "meshobject.h"
#if !defined(QT_OPENGL_ES_1)
#include "perpixeleffect.h"
#endif
#include <QGraphicsScene>

class SceneManager : public QObject
{
    Q_OBJECT
public:
    SceneManager(QString siteFilePath, QObject *parent=0);
    ~SceneManager();

    SceneObject *scene;

    Sphere *sphere;

    QGLMaterial *china;
    QGLMaterial *chinaHighlight;
    QGLMaterial *metal;
    QGLMaterial *metalHighlight;
    QGLMaterial *rubMaterial;

#if !defined(QT_OPENGL_ES_1)
    PerPixelEffect *lighting;
#endif
    QList<MeshObject*> placesList;

    void changeMaterials(bool perPixel);
    void loadSite(QString sitePath);

    QGLSvg2Scene *svgScene;
    QSize sceneSize;
    QVector3D pos3d;
    QString error;
    QGraphicsScene *graphicsScene;
    QMap<QGLSceneNode*, QGraphicsRectItem*> *sceneNodeGItemMap;

signals:
    void changed();

private slots:
    void objClicked();
    void objHovered();
    void sphereClicked();

private:
};

#endif // SCENEMANAGER_H

