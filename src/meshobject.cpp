/****************************************************************************
**
** Copyright (C) 2011 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of the Qt3D module of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the Technology Preview License Agreement accompanying
** this package.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**
**
**
**
**
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "meshobject.h"
#include <Qt3D/qglview.h>
#include <Qt3D/qglsphere.h>
#include <Qt3D/qglbuilder.h>
#include <Qt3D/qglsvg2scene.h>

MeshObject::MeshObject(QGLSceneNode *meshObject, QObject *parent)
    : QObject(parent)
{
    m_mesh = 0;
    m_meshObject = meshObject;
    m_scale = 1.0f;
    m_rotationAngle = 0.0f;
    m_effect = 0;
    m_objectId = -1;
    m_hovering = false;
    m_selected = false;
    m_material = 0;
    m_hoverMaterial = 0;
    m_selectionMaterial = 0;
    m_transparent = false;
    //m_rubberBand = false;
    m_standardEffect = QGL::LitModulateTexture2D;
}

MeshObject::MeshObject(QGLAbstractScene *scene, QObject *parent)
    : QObject(parent)
{
    scene->setParent(this);
    m_mesh = 0;
    m_meshObject = scene->mainNode();
    m_scale = 1.0f;
    m_rotationAngle = 0.0f;
    m_effect = 0;
    m_objectId = -1;
    m_hovering = false;
    m_selected = false;
    m_material = 0;
    m_hoverMaterial = 0;
    m_selectionMaterial = 0;
    m_transparent = false;
    //m_rubberBand = false;
    m_standardEffect = QGL::LitModulateTexture2D;
}

MeshObject::~MeshObject()
{
    delete m_mesh;
}

void MeshObject::initialize(QGLView *view, QGLPainter *painter)
{
    Q_UNUSED(painter);
    if (m_objectId != -1)
        view->registerObject(m_objectId, this);
}

void MeshObject::draw(QGLPainter *painter)
{
    // Position the model at its designated position, scale, and orientation.
    //if(!m_rubberBand)
    {
    painter->modelViewMatrix().push();
    painter->modelViewMatrix().translate(m_position);

    if(m_scale != 1.0f)
        painter->modelViewMatrix().scale(m_scale);

    if(m_rotationAngle != 0.0f)
        painter->modelViewMatrix().rotate(m_rotationAngle, m_rotationVector);
    }

    // Apply the material and effect to the painter.
    QGLMaterial *material;
    if (m_hovering)
        material = m_hoverMaterial;
    else
        if(m_selected)
        {
            if(m_selectionMaterial)
                material = m_selectionMaterial;
            else
                material = m_hoverMaterial;

            m_meshObject->setMaterialIndex(1);
        }
        else
        {
            material = m_material;
            m_meshObject->setMaterialIndex(0);
        }

    if(m_transparent)
    {
        glEnable(GL_BLEND);
        painter->setFaceColor(QGL::AllFaces, material->diffuseColor());
    }
    else
    {
        glDisable(GL_BLEND);
        painter->setColor(material->diffuseColor());
        painter->setFaceMaterial(QGL::AllFaces, material);
    }

    if (m_effect)
        painter->setUserEffect(m_effect);
    else
        painter->setStandardEffect(m_standardEffect);

    // Mark the object for object picking purposes.
    /*int prevObjectId = painter->objectPickId();
    if (m_objectId != -1)
        painter->setObjectPickId(m_objectId);*/

    /*if(m_rubberBand)
    {
        //glPushMatrix();
        //glLoadIdentity();
        //painter->modelViewMatrix().push();
        //painter->modelViewMatrix().setToIdentity();
        painter->projectionMatrix().push();
        painter->projectionMatrix().rotate(-m_rotationAngle, m_rotationVector);//.setToIdentity();
    }*/
    // Draw the geometry mesh.
    if(m_meshObject)
        m_meshObject->draw(painter);
    else
        m_mesh->draw(painter);

    /*if(m_rubberBand)
    {
        //painter->modelViewMatrix().pop();
        painter->projectionMatrix().pop();
        //glPopMatrix();
    }*/

    // Turn off the user effect, if present.
    if(m_effect)
        painter->setStandardEffect(m_standardEffect);

    // Revert to the previous object identifier.
    //painter->setObjectPickId(prevObjectId);

    // Restore the modelview matrix.
    //if(!m_rubberBand)
    painter->modelViewMatrix().pop();
}

bool MeshObject::event(QEvent *e)
{
    // Convert the raw event into a signal representing the user's action.
    if (e->type() == QEvent::MouseButtonPress) {
        QMouseEvent *me = (QMouseEvent *)e;
        if (me->button() == Qt::LeftButton)
            emit pressed();
    } else if (e->type() == QEvent::MouseButtonRelease) {
        QMouseEvent *me = (QMouseEvent *)e;
        if (me->button() == Qt::LeftButton) {
            emit released();
            if (me->x() >= 0)   // Positive: inside object, Negative: outside.
                emit clicked();
        }
    } else if (e->type() == QEvent::MouseButtonDblClick) {
        emit doubleClicked();
    } else if (e->type() == QEvent::Enter) {
        m_hovering = true;
        emit hoverChanged();
    } else if (e->type() == QEvent::Leave) {
        m_hovering = false;
        emit hoverChanged();
    }
    return QObject::event(e);
}

static QGLSceneNode *createSphere(QObject *parent)
{
    QGLBuilder builder;
    builder << QGLSphere(.2);

    QGLSceneNode *n = builder.finalizedSceneNode();
    n->setParent(parent);

    return n;
}

Sphere::Sphere(QObject *parent)
    : MeshObject(createSphere(parent), parent)
{
}


