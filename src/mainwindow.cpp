#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QFileDialog>
#include <QMessageBox>
#include <QFile>
#include <QMdiSubWindow>
#include <QSettings>
#include <QTextStream>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    setCentralWidget(ui->mdiArea);

    //setMinimumSize(1905, 960);
    //setMinimumSize(800, 600);
    move(0,0);
    setWindowTitle(QLatin1String("Editeur de sites SimpleCLIC"));

/*    QMenu *menu = this->menuBar()->addMenu(QLatin1String("Effects"));

    standardLighting = new QAction(QLatin1String("Standard lighting"), this);
    menu->addAction(standardLighting);

#if !defined(QT_OPENGL_ES_1)
    perPixelLighting = new QAction(QLatin1String("Per-pixel lighting"), this);
    menu->addAction(perPixelLighting);
#endif

    menu->addSeparator();

    QAction *exitAction = new QAction(QLatin1String("E&xit"), this);
    menu->addAction(exitAction);
    QObject::connect(exitAction, SIGNAL(triggered()), qApp, SLOT(quit()));*/

    QFileInfo fInfo(qApp->arguments()[0]);
    curPath = fInfo.absolutePath() + "/"; // directorio de trabajo

    //server = "localhost";
    //port = 8084;

    createActions();
    createMenus();

    stateIconsCache = new QList< QMap<int, QMap<QString, QPair<QImage, QImage> > > >;
}

MainWindow::~MainWindow()
{
    delete ui;
    delete stateIconsCache;
}

void MainWindow::setStatusBarMessage(QString msg)
{
    ui->statusBar->showMessage(msg, 2000);
}

void MainWindow::createActions()
{
    //Acciones del menu File
    openAct = new QAction(QIcon(":/iconos/fileopen.png"),tr("&Ouvrir"), this);
    openAct->setStatusTip(tr("Ouvrir un plan existant"));
    openAct->setShortcut(QKeySequence::Open);
    connect(openAct, SIGNAL(triggered()), this, SLOT(open()));

    saveAct = new QAction(QIcon(":/iconos/filesave.png"),tr("&Save"), this);
    saveAct->setStatusTip(tr("Save the document to disk"));
    saveAct->setShortcut(QKeySequence::Save);
    saveAct->setEnabled(0);
    connect(saveAct, SIGNAL(triggered()), this, SLOT(save()));

    saveAsAct = new QAction(QIcon(":/iconos/filesaveas.png"),tr("Enregistrer &sous..."), this);
    saveAsAct->setShortcut(Qt::CTRL+Qt::Key_U);
    saveAsAct->setStatusTip(tr("Enregistrer le plan sous un autre nom"));
    saveAsAct->setEnabled(0);
    connect(saveAsAct, SIGNAL(triggered()), this, SLOT(saveAs()));

    exportAct = new QAction(tr("&Exporter vers .sit"), this);
    exportAct->setStatusTip("Exporter vers .sit");
    //exportAct->setShortcut(Qt::CTRL+Qt::SHIFT+Qt::Key_S);
    exportAct->setEnabled(0);
    connect(exportAct, SIGNAL(triggered()), this, SLOT(exportToSit()));

    saveAllAct = new QAction(tr("&Enregistrer tout"), this);
    saveAllAct->setStatusTip("Enregistrer tous les plans");
    saveAllAct->setShortcut(Qt::CTRL+Qt::SHIFT+Qt::Key_S);
    saveAllAct->setEnabled(0);
    connect(saveAllAct,SIGNAL(triggered()), this, SLOT(saveAll()));

    closeAct = new QAction(QIcon(":/iconos/fileclose.png"),tr("&Close"), this);
    closeAct->setStatusTip(tr("Fermer la fenêtre active"));
    closeAct->setShortcut(Qt::CTRL+Qt::Key_W);
    closeAct->setEnabled(0);
    connect(closeAct, SIGNAL(triggered()), this, SLOT(closeSubWindow()));

    closeAllAct = new QAction(tr("C&lose All"), this);
    closeAllAct->setStatusTip(tr("Fermer toutes les fenêtres"));
    closeAllAct->setShortcut(Qt::CTRL+Qt::SHIFT+Qt::Key_W);
    closeAllAct->setEnabled(0);
    connect(closeAllAct, SIGNAL(triggered()), this, SLOT(closeAll()));

    exitAct = new QAction(QIcon(":/iconos/exit.png"),tr("&Quitter"), this);
    exitAct->setShortcut(Qt::CTRL+Qt::Key_Q);
    exitAct->setStatusTip(tr("Quitter l'application"));
    connect(exitAct, SIGNAL(triggered()), qApp, SLOT(closeAllWindows()));

    for(int i = 0; i < MaxRecentFiles; ++i)
    {
        recentFileActs[i] = new QAction(this);
        recentFileActs[i]->setVisible(false);
        connect(recentFileActs[i], SIGNAL(triggered()), this, SLOT(openRecentFile()));
    }

    //Ayuda
    indexAct = new QAction(QIcon(":/iconos/help.png"),tr("&Aide"), this);
    indexAct->setShortcut(Qt::Key_F1);
    indexAct->setStatusTip(tr("Aide"));
    connect(indexAct, SIGNAL(triggered()), this, SLOT(about()));

    aboutSiteEditorAct = new QAction(tr("A propos de l'&Editeur"), this);
    aboutSiteEditorAct->setStatusTip(tr("A propos de l'&Editeur"));
    connect(aboutSiteEditorAct, SIGNAL(triggered()), this, SLOT(about()));
}

void MainWindow::createMenus()
{
    recentFilesMenu = new QMenu("Fichiers &récents");
    for(int i = 0; i < MaxRecentFiles; ++i)
        recentFilesMenu->addAction(recentFileActs[i]);

    fileMenu = ui->menuBar->addMenu(tr("&Fichier"));
    fileMenu->addAction(openAct);
    fileMenu->addAction(saveAct);
    fileMenu->addAction(saveAsAct);
    fileMenu->addAction(saveAllAct);
    fileMenu->addAction(exportAct);
    fileMenu->addSeparator();
    fileMenu->addAction(closeAct);
    fileMenu->addAction(closeAllAct);
    fileMenu->addSeparator();
    fileMenu->addAction(exitAct);

    updateRecentFileActions();

    //Menu ayuda
    helpMenu = menuBar()->addMenu(tr("&Aide"));
    helpMenu->addAction(indexAct);
    helpMenu->addAction(aboutSiteEditorAct);
}

void MainWindow::updateRecentFileActions()
{
    QSettings settings("SimpleCLIC", "SiteEditor");
    QStringList files;
    if(settings.contains("recentFileList"))
        files = settings.value("recentFileList").toStringList();

    int numRecentFiles = qMin(files.size(), (int)MaxRecentFiles);

    for (int i = 0; i < numRecentFiles; ++i)
    {
        QString text = tr("&%1 %2").arg(i + 1).arg(QFileInfo(files[i]).fileName());
        recentFileActs[i]->setText(text);
        recentFileActs[i]->setData(files[i]);
        recentFileActs[i]->setVisible(true);
    }
    for(int j = numRecentFiles; j < MaxRecentFiles; ++j)
        recentFileActs[j]->setVisible(false);

    if(numRecentFiles > 0)
        fileMenu->insertMenu(saveAct, recentFilesMenu);
}

void MainWindow::open()
{
    QString fileNameIn = QFileDialog::getOpenFileName(this, tr("Ovrir un fichier SVG"), curPath, tr("Fichiers SVG (*.svg)"));
    if(fileNameIn.isNull())
        return;

    if(!QFile::exists(fileNameIn))
    {
        QMessageBox::information(0, tr("Erreur"), tr("Le fichier indiqué n'existe pas!"));
        return;
    }

    if(loadFile(fileNameIn))
        setCurrentFile(fileNameIn);
}

void MainWindow::setCurrentFile(const QString &fileName)
{
    setWindowFilePath(fileName);

    QSettings settings("SimpleCLIC", "SiteEditor");
    QStringList files = settings.value("recentFileList").toStringList();

    if(!files.contains(fileName))
        files.prepend(fileName);

    if(files.size() > MaxRecentFiles)
        files.removeLast();

    settings.setValue("recentFileList", files);

    updateRecentFileActions();
}

void MainWindow::openRecentFile()
{
    QAction *action = qobject_cast<QAction *>(sender());
    if(action)
    {
        QString fileName = action->data().toString();

        bool r = loadFile(fileName);

        QSettings settings("SimpleCLIC", "SiteEditor");
        QStringList files = settings.value("recentFileList").toStringList();

        files.removeAll(fileName);

        if(r)
            files.prepend(fileName);

        settings.setValue("recentFileList", files);
        updateRecentFileActions();
    }
}

#include <QTime>
bool MainWindow::loadFile(QString fileName)
{
    //verificar si el fichero está cargado
    QList<QMdiSubWindow*> mt = ui->mdiArea->subWindowList();
    Site2dView *siteView;
    for(int i=0,n=mt.size(); i<n; i++)
    {
        siteView = (Site2dView*)mt.at(i)->widget();
        if(siteView->svgFile.compare(fileName)==0)
        {
            ui->mdiArea->setActiveSubWindow(mt.at(i));
            return true;
        }
    }

    setCursor(Qt::WaitCursor);
    QTime t0 = QTime::currentTime();
    siteView = new Site2dView(0, true, true, this);
    siteView->setStateIconsCache(stateIconsCache);
    siteView->loadPlan(&QFile(fileName));
    QString errMsg = siteView->errorMsg();
    if(!errMsg.isEmpty())
    {
        QMessageBox::critical(this, qApp->applicationName(), errMsg);
        setCursor(Qt::ArrowCursor);
        return false;
    }
    siteView->setAttribute(Qt::WA_DeleteOnClose);
    ui->mdiArea->addSubWindow(siteView);
    siteView->showMaximized();
    int n = t0.msecsTo(QTime::currentTime());
    qDebug(QString("Cargado en %1 segundos").arg(n/1000.0).toUtf8().constData());
    setCursor(Qt::ArrowCursor);

    saveAct->setEnabled(1);
    saveAsAct->setEnabled(1);
    saveAllAct->setEnabled(1);
    exportAct->setEnabled(1);
    closeAct->setEnabled(1);
    closeAllAct->setEnabled(1);

    return true;
}

void MainWindow::save()
{
    //determinando ventana activa
    QMdiSubWindow * mt =  ui->mdiArea->activeSubWindow();
    if(mt==0)
        return;

    Site2dView *siteView = (Site2dView*)mt->widget();
    siteView->removeFileWatcher();

    //salvar el fichero
    if(siteView->save())
    {
        siteView->setSaved();
        statusBar()->showMessage(tr("File saved"), 2000);
    }

    siteView->updateFileWatcher();
}

void MainWindow::saveAs()
{
    //Obtener ventana activa
    QMdiSubWindow * mt =  ui->mdiArea->activeSubWindow();
    if(mt==0)
        return;
    Site2dView *siteView = (Site2dView*)mt->widget();

    QString fileNameSave = QFileDialog::getSaveFileName(this, tr("Choisissez le nouveau nom du fichier"), curPath+"/"+siteView->getSiteDialogName(), tr("Fichiers SVG (*.svg)"));
    if(fileNameSave.isNull())
        return;

    siteView->removeFileWatcher();

    //salvar el fichero
    if(siteView->save(fileNameSave))
    {
        siteView->svgFile = fileNameSave;
        siteView->setSaved();
        statusBar()->showMessage(tr("File saved"), 2000);
    }

    siteView->updateFileWatcher();
}

void MainWindow::saveAll()
{
    QList<QMdiSubWindow *> mt = ui->mdiArea->subWindowList();
    for(int i=0, n=mt.size(); i<n; i++)
    {
        Site2dView *siteView = (Site2dView*)mt.at(i)->widget();
        siteView->removeFileWatcher();
        if(siteView->save())
            siteView->setSaved();
        siteView->updateFileWatcher();
    }
}

void MainWindow::exportToSit()
{
    //Obtener ventana activa
    QMdiSubWindow * mt =  ui->mdiArea->activeSubWindow();
    if(mt==0)
        return;
    Site2dView *siteView = (Site2dView*)mt->widget();

    //QString fileNameSave = QFileDialog::getSaveFileName(this, tr("Choisissez le nom du fichier .sit"), /*curPath+"/"+siteView->getSiteDialogName()*/"D:/Yovanis/Emplois/MaPlace/SiteConfImporter/plansNouvelleVersion", tr("Fichiers SIT (*.sit)"));
    QString fileNameSave = QFileDialog::getSaveFileName(this, tr("Choisissez le nom du fichier .sit"), /*curPath+"/"+siteView->getSiteDialogName()*/"D:/SiteConfImporter/plansNouvelleVersion", tr("Fichiers SIT (*.sit)"));
    if(fileNameSave.isNull())
        return;

    siteView->exportToSit(fileNameSave);
}

void MainWindow::closeSubWindow()
{
    ui->mdiArea->closeActiveSubWindow();
}

void MainWindow::closeAll()
{
    ui->mdiArea->closeAllSubWindows();
}

void MainWindow::about()
{
    QMessageBox::about(this, tr("About Site Editor"), tr("The <b>Site Editor</b> permits... "));
}

/*void MainWindow::pbLoadFile3DClic()
{
    QString fileNameIn = QFileDialog::getOpenFileName(this, tr("fichier SVG"), curPath+"/svg", tr("Fichiers SVG (*.svg)"));

    if(fileNameIn.isNull())
        return;

    if(!QFile::exists(fileNameIn))
    {
        QMessageBox::information(0, tr("Error"), tr("Le fichier de configuration de site n'existe pas"));
        return;
    }

    this->setCursor(Qt::WaitCursor);
    QTime t0 = QTime::currentTime();
    siteView = new SiteView(fileNameIn, this);
    if(siteView->error().isEmpty())
    {
        siteView->setAttribute(Qt::WA_DeleteOnClose);
        ui->mdiArea->addSubWindow(siteView);
        //siteView->setMinimumSize(1890, 875);
        siteView->move(0,0);
        connect(standardLighting, SIGNAL(triggered()), siteView, SLOT(standardLighting()));
    #if !defined(QT_OPENGL_ES_1)
        connect(perPixelLighting, SIGNAL(triggered()), siteView, SLOT(perPixelLighting()));
    #endif
        siteView->show();
        int n = t0.msecsTo(QTime::currentTime());
        qDebug(QString("Cargado en %1 segundos").arg(n/1000.0).toAscii().constData());
    }
    else
    {
        QMessageBox::information(0, tr("Error"), tr(siteView->error().toAscii().constData()));
        delete siteView;
    }
    this->setCursor(Qt::ArrowCursor);
}*/

