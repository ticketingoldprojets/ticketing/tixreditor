#ifndef SITEDIALOG_H
#define SITEDIALOG_H

#include <QDialog>
#include <QGraphicsView>
#include <QGraphicsSvgItem>
#include <QDomDocument>
#include <QSvgRenderer>
#include <QList>
#include <QNetworkReply>
#include <QNetworkAccessManager>
#include <QBuffer>
#include <QTreeWidgetItem>
#include <QMenu>
#include <QSplitter>

namespace Ui {
    class SiteDialog;
}

class ScGraphicsSvgItem : public QGraphicsSvgItem
{
    Q_OBJECT

public:
    ScGraphicsSvgItem(QGraphicsItem * parent = 0);
    ScGraphicsSvgItem(const QString & fileName, QGraphicsItem * parent = 0);
    //~ScGraphicsSvgItem();

    /*QRectF boundingRect() const;
    QPainterPath shape() const;*/
    void paint( QPainter * painter, const QStyleOptionGraphicsItem * option, QWidget * widget = 0 );
    void changeIsPlaceFnac(bool newState);

    enum { Type = UserType + 1 };

    int type() const
    {
        return Type;
    }

private:
    bool isSelected;
    bool isPlaceFnac;
    QColor selectedColor;
};

class SiteDialog;

class ScGraphicsView_OLD : public QGraphicsView
{
    Q_OBJECT

public:
    ScGraphicsView_OLD(QString cPath, QWidget * parent = 0);
    //~ScGraphicsView_OLD();
    void zoomScene(double factor);
    void rotScene(int angle);
    QList<ScGraphicsSvgItem*> listSelectedSvgItem;
    bool viewportEvent(QEvent *event);
    SiteDialog *siteDialog;

protected:
    void contextMenuEvent ( QContextMenuEvent * event );
    void mouseMoveEvent(QMouseEvent *event);

private:
    QString newPaintedItem(QString fileOriginalItem, QColor color, QString elemId);
    void insertListSelectedSvgItem(ScGraphicsSvgItem *svgItem);

    ScGraphicsSvgItem *svgItemRemoved;
    QStringList listSelectedSvgItemId;
    QString curPath;
    QPointF iniPoint;
    QRectF iniRect;
    QGraphicsItem *selectorItem;
    int actualRotationAngle;
    qreal totalScaleFactor;
    int totalAngle;

    QMenu popupMenu;
    int modeSelection;
    bool usePhysicalCoord;
    bool multiSelec;

private slots:
    void changerModeSelection();
    void changerUsePhysicalCoord();
    void itemsSelectionChanged();
};

class SiteDialog : public QDialog
{
    Q_OBJECT

public:
    explicit SiteDialog(QString curPath, QString svgFileName, double scaleFactor, QWidget *parent = 0);
    ~SiteDialog();
    void selectItem(QString itemId);
    void unSelectItem(QString itemId);
    void unSelectAllItems();
    void showSelectedItemsId(QStringList itemIdList);
    QDomDocument svgDomDoc;
    QMap<QString,QDomElement> idDomElemMap;
    QMap<QString,ScGraphicsSvgItem*> idScGraphicsSvgItemMap;
    QWidget *mainWindow;
    QStringList selectedItemsIdList;
    bool setNewZoomValue(qreal value);

private:
    Ui::SiteDialog *ui;
    ScGraphicsView_OLD *graphicsView;
    QStringList idList;
    QStringList tmpIdList;
    QList< QList<QTreeWidgetItem *> > treeItemSelectionsList;

    QSvgRenderer *renderer;

    QNetworkAccessManager qnam;
    QNetworkReply *reply;
    QBuffer replyBuffer;
    QDomDocument responseDOM;
    bool canBePainted;
    void domTreeWalker(QDomElement root);
    QString curPath;
    QString siteName;
    QString confSalleFileName;
    bool makeRequest;

    int totalBlocs;
    int totalPlaces;

    QSplitter *splitter;

private slots:
    void zoomScene(double factor);
    void rotScene(int angle);
    void rotScene2(int angle);
    void downLoadBtnClick();
    void httpFinished();
    void httpFinished2();
    void httpReadyRead();
    void affecterBtnClick();
    void eliminerBtnClick();
    void genererBtnClick();
    void showCountSelectedPlaces();
};

#endif // SITEDIALOG_H

