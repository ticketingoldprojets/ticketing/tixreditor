#include <qglobal.h>
#if (QT_VERSION < QT_VERSION_CHECK(5, 0, 0))
#include <QtGui/QApplication>
#else
#include <QtWidgets/QApplication>
#endif

#include "mainwindow.h"

#include <QtCore/QFileInfo>
#include <QtCore/qmath.h>
#include <QtCore/QResource>
#include <QtCore/QDir>

#if (QT_VERSION < QT_VERSION_CHECK(5, 0, 0))
#include <QtGui/QMessageBox>
#else
#include <QtWidgets/QMessageBox>
#endif

void genScaledPict(QString fileName)
{
    QFileInfo fi(fileName);

    QSvgRenderer svgRend(fileName);
    for(int i=4; i<9; i++)
    {
        int n = qPow(2,i);
        QImage img(n, n, QImage::Format_RGB32);
        img.fill(0);
        QPainter painter(&img);
        svgRend.render(&painter);
        QString f = QString("%1/%2%3x%3.png").arg(fi.path()).arg(fi.baseName()).arg(n);
        img.save(f);
    }
}

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    a.setApplicationName("TixRealm Editor");
    a.setWindowIcon(QIcon(":/sc.png"));

    //!<Aplicar el skin, por el momento es sólo un agestión rudimentaria de skin. Más adelante habrá que permitir elegir uno de entre varios skin, incluso luego de haber iniciado la aplicación.
    QFileInfo fInfo(qApp->arguments()[0]);
    QString curPath = fInfo.absolutePath() + QDir::separator();

    QString skinFilename = curPath + "default.skin";
    if(!QFile::exists(skinFilename))
    {
        skinFilename = ":/skin/default.skin";
    }

    //registrar el nuevo recurso
    if(!QResource::registerResource(skinFilename))
    {
        QMessageBox::critical(0, a.applicationName(), QObject::tr("Cannot load skin file"));
        return 3;
    }

    //cargar el Style
    QFile file(":/styles.qss");
    if(file.open(QFile::ReadOnly))
    {
        a.setStyleSheet(QLatin1String(file.readAll()));
        file.close();
    }

    MainWindow mainw;

    //genScaledPict("D:/SimpleCLICsiteEditor/placeLibre.svg");
    //genScaledPict("D:/SimpleCLICsiteEditor/placeOccupee.svg");

    mainw.showMaximized();
    return a.exec();
}

