#ifndef SITEVIEW_H
#define SITEVIEW_H

#include <Qt3D/qglview.h>
#include "scenemanager.h"
#include <QGraphicsScene>
#include <QRubberBand>

class CustomRB : public QRubberBand
{
public:
    CustomRB(Shape s, QWidget *parent);

protected:
    void paintEvent(QPaintEvent *event);
};

class SiteView : public QGLView
{
    Q_OBJECT
public:
    SiteView(QString siteFilePath, QWidget *parent = 0);
    QWidget *mainWindow;
    QString error() {return sceneManager->error;}

signals:

public slots:
    void standardLighting();
    void perPixelLighting();

protected:
    void initializeGL(QGLPainter *painter);
    void paintGL(QGLPainter *painter);
    void keyPressEvent(QKeyEvent *e);
    void mousePressEvent(QMouseEvent *e);
    void mouseMoveEvent(QMouseEvent *e);
    void mouseReleaseEvent(QMouseEvent *e);

private:
    void screenToGL(QPoint pos, QPoint pos1, QVector3D *vect, QVector3D *vect1);
    void unSelectAllItems();
    SceneManager *sceneManager;
    QGLLightModel *m_lightModel;
    QGLLightParameters *m_lightParameters;
    QPoint m_pos;
    bool lfBtnPressed;
    //MeshObject *rubberBand;
    QList<QObject*> *selObjsList;

    bool panning;
    QPoint startPan;
    QPoint lastPan;
    QVector3D startEye;
    QVector3D startCenter;
    QVector3D startUpVector;
    //void wheel(int delta);
    void pan(int deltax, int deltay);
    void rotate(int deltax);
    qreal m_angle;
    //QQuaternion quat;
    //QQuaternion quat2;
    CustomRB *rubberBand2;
};

#endif // SITEVIEW_H

