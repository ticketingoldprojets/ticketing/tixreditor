#include "scenemanager.h"
#include <QGraphicsRectItem>

SceneManager::SceneManager(QString siteFilePath, QObject *parent)
    : QObject(parent)
{
    scene = new SceneObject(this);

    loadSite(siteFilePath);

    china = new QGLMaterial(this);
    china->setAmbientColor(QColor(192, 150, 128));
    china->setSpecularColor(QColor(60, 60, 60));
    china->setShininess(128);

    chinaHighlight = new QGLMaterial(this);
    chinaHighlight->setAmbientColor(QColor(255, 192, 0));
    chinaHighlight->setSpecularColor(QColor(60, 60, 0));
    chinaHighlight->setShininess(128);
    chinaHighlight->setTextureCombineMode(QGLMaterial::Replace);

    metal = new QGLMaterial(this);
    metal->setAmbientColor(QColor(255, 255, 255));
    metal->setDiffuseColor(QColor(150, 150, 150));
    metal->setSpecularColor(QColor(255, 255, 255));
    metal->setShininess(128);

    metalHighlight = new QGLMaterial(this);
    metalHighlight->setAmbientColor(QColor(255, 255, 96));
    metalHighlight->setDiffuseColor(QColor(150, 150, 96));
    metalHighlight->setSpecularColor(QColor(255, 255, 255));
    metalHighlight->setShininess(128);

    rubMaterial = new QGLMaterial(this);
    rubMaterial->setDiffuseColor(QColor(210, 50, 242, 200));

    sphere = new Sphere(scene);
    scene->addMember(sphere);
    sphere->setPosition(QVector3D(0.0f, 0.0f, 0.0f));
    sphere->setObjectId(ObjSphere1);
    sphere->setStandardEffect(QGL::LitMaterial);

#if !defined(QT_OPENGL_ES_1)
    lighting = new PerPixelEffect();
#endif
    changeMaterials(false);

    connect(sphere, SIGNAL(hoverChanged()), this, SIGNAL(changed()));
    connect(sphere, SIGNAL(clicked()), this, SLOT(sphereClicked()));
}

SceneManager::~SceneManager()
{
#if !defined(QT_OPENGL_ES_1)
    delete lighting;
#endif
}

void SceneManager::loadSite(QString sitePath)
{
    QString options = "QGLWidgetAdr="+QString::number((ulong)parent());
    svgScene = (QGLSvg2Scene*)QGLAbstractScene::loadScene(/*QUrl::fromLocalFile(sitePath)*/sitePath, QString(), options);
    error.clear();
    if(!svgScene)
    {
        error = "Could not load " + sitePath + ", probably plugin could not be found.";
        return;
    }

    svgScene->setParent(this);
    QGLSceneNode *mNode = svgScene->mainNode();

    QSize wSize = ((QGLWidget*)parent())->minimumSize();//size();
    sceneSize = svgScene->bufferSize;

    qreal scale = 1.0;
    if(sceneSize.width()>wSize.width() && sceneSize.height()>wSize.height())
    {
        scale = qreal(wSize.width())/sceneSize.width();
        qreal aux = qreal(wSize.height())/sceneSize.height();
        if(scale>aux)
            scale = aux;
    }
    else
        if(sceneSize.width()>wSize.width())
        {
            scale = qreal(wSize.width())/sceneSize.width();
        }
        else
            if(sceneSize.height()>wSize.height())
            {
                scale = qreal(wSize.height())/sceneSize.height();
            }

    sceneSize.setWidth(scale*sceneSize.width());
    sceneSize.setHeight(scale*sceneSize.height());

    pos3d = QVector3D(-sceneSize.width()/2., sceneSize.height()/2., -1.0f);

    qDebug(QString("scale=%1, X=%2, Y=%3, Z=%4").arg(QString::number(scale), QString::number(pos3d.x()), QString::number(pos3d.y()), QString::number(pos3d.z())).toAscii().constData());

    graphicsScene = svgScene->graphicsScene;
    sceneNodeGItemMap = svgScene->sceneNodeGItemMap;
    QList<QGLSceneNode*> allChildrenList = mNode->allChildren();
    int i = 1;
    foreach(QGLSceneNode *node, allChildrenList)
    {
        MeshObject *mObj = new MeshObject(node, scene);
        mObj->setObjectId(i++);
        mObj->setPosition(pos3d);
        mObj->setScale(scale);
        mObj->setStandardEffect(QGL::LitModulateTexture2D);
        mObj->setScene(svgScene);
        connect(mObj, SIGNAL(hoverChanged()), this, SIGNAL(changed()));
        //connect(mObj, SIGNAL(hoverChanged()), this, SLOT(objHovered()));
        connect(mObj, SIGNAL(clicked()), this, SLOT(objClicked()));
        placesList.append(mObj);
        scene->addMember(mObj);
        QGraphicsRectItem *gRectItem = sceneNodeGItemMap->value(node);
        gRectItem->setData(0, QVariant::fromValue(mObj));
    }
    delete sceneNodeGItemMap;
}

void SceneManager::changeMaterials(bool perPixel)
{
    sphere->setMaterial(metal);
    sphere->setHoverMaterial(metalHighlight);
#if !defined(QT_OPENGL_ES_1)
    if (perPixel) {
        sphere->setEffect(lighting);
    } else
#endif
    {
        sphere->setEffect(0);
    }

    foreach(MeshObject *mObj, placesList)
    {
        mObj->setMaterial(metal);
        mObj->setHoverMaterial(metalHighlight);
        mObj->setSelectionMaterial(chinaHighlight);
        #if !defined(QT_OPENGL_ES_1)
        if(perPixel)
        {
            mObj->setEffect(lighting);
        }
        else
        #endif
        {
            mObj->setEffect(0);
        }
    }
}

void SceneManager::sphereClicked()
{
    qDebug("sphere clicked");
}

void SceneManager::objClicked()
{
    QObject *obj = sender();
    QString str = "place UNKNOWN clicked";
    if(obj)
        str = "place " + QString::number(((MeshObject*)obj)->objectId()) + " clicked";
    qDebug(str.toAscii().constData());
}

void SceneManager::objHovered()
{
    QObject *obj = sender();
    QString str = "place UNKNOWN hovered";
    if(obj)
        str = "place " + QString::number(((MeshObject*)obj)->objectId()) + " hovered";
    qDebug(str.toAscii().constData());
    emit this->changed();
}

