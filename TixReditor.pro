#-------------------------------------------------
#
# Project created by QtCreator 2011-04-15T12:53:21
#
#-------------------------------------------------

QT       += core svg xml network opengl

greaterThan(QT_MAJOR_VERSION, 4) {
        INCLUDEPATH += $$(INCLUDEPATHQt5)
        QT += widgets uitools
    } else {
        INCLUDEPATH += $$(INCLUDEPATHQt4)
        QT += gui
        CONFIG += uitools
    }

TARGET = TixReditor
TEMPLATE = app

INCLUDEPATH += ../ScGraphicsViewPlugin

SOURCES += src/main.cpp\
        src/mainwindow.cpp\
        ../TixRealmClient/src/scmetaqenum.cpp

HEADERS  += src/mainwindow.h \
            ../ScGraphicsViewPlugin/scgraphicsview.h \
            ../TixRealmClient/src/scmetaqenum.h

FORMS    += assets/ui/mainwindow.ui

RESOURCES += assets/resources.qrc

include(../ClientEditorSharedSources/clienteditorsharedsources.pri)

CONFIG(debug, debug|release)
{
OBJECTS_DIR = debug/obj/.obj
MOC_DIR = debug/obj/.moc
RCC_DIR = debug/obj/.rcc
UI_DIR = debug/obj/.ui
}

CONFIG(release, debug|release)
{
OBJECTS_DIR = release/obj/.obj
MOC_DIR = release/obj/.moc
RCC_DIR = release/obj/.rcc
UI_DIR = release/obj/.ui
}

DEFINES += TIXREDITOR=1
